# README

## How to submit your code assignment

1. Fork the project from GitLab.
2. Make your forked project to **private** from GitLab -> Settings -> General -> Permissions -> Project visibility.
3. Add member of **singtel_shijian** in **Select members to invite**, and choose the role permission to be **Developer** from GitLab -> Settings -> Members.
4. Complete your code and documentation in the forked project.
5. Upload your resume or CV in the root folder of the forked project.
6. Create a new plain text file in the root folder of the forked project. And make the file name in the format of "your employer + your full name", e.g. **Your Employer Name + John Doe**.
7. Update the forked project description in GitLab to "your employer + full name", e.g. **Your Employer Name + John Doe** from GitLab -> Settings -> General -> General project -> Project description.  
8. Notify the code assignment is done.

## Code assignment details

### Question 1

According to the description in [Release and Deployment Management](https://wiki.en.it-processmaps.com/index.php/Release_and_Deployment_Management#Release),

> A Release (also referred to as a Release Package) consists of a single Release Unit or a structured set of Release Units.

Please describes the details of **Release** in your current or previous project, and point it out which part is under your responsibilities.

### Question 2

Please describes the details of below processes in your current or previous project.

It can be provided the information of tools, steps, approaches, pipeline or action items, etc.  

- Release Planning
- Release Build
- Release Deployment
- Release Closure

### Question 3

Based on the answer of **Question 2**, please describes what is the automation in your selected project.

If there is lacking at the automation, what is your idea to improve the automation in the release management.

### Question 4 (Bonus Question)

Please describes how to create the **AWS EC2** instance with the automated script.

This question requires to provide the **code snippets** for explanation.

### Question 5 (Bonus Question)

Please describes how to management the release version of JAR artifacts in Maven or Gradle.

This question requires to provide the **code snippets** for explanation.
